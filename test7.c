//Develop a C program to read and print employee details using structures
#include<stdio.h>
struct employee
{
int empid;
char empname[15];
int emprank;
int empsal;
};
int main()
{
struct employee e;
printf("Please enter the Employee details.\n");
printf("Employee ID:");
scanf("%d",&e.empid);
printf("Employee Name:");
scanf("%s",e.empname);
printf("Employee Rank:");
scanf("%d",&e.emprank);
printf("Employee Salary:");
scanf("%d",&e.empsal);
printf("\n");
printf("THE EMPLOYEE DETAILS ARE AS FOLLOWS.\n");
printf("Employee ID:%d\n",e.empid);
printf("Employee Name:%s\n",e.empname);
printf("Employee Rank:%d\n",e.emprank);
printf("Employee Salary:%d\n",e.empsal);
return 0;
}
