/*1) C program to find sum of all natural numbers. 
Series: 1+2+3+4+..N*/
#include<stdio.h>
int main()
{
int i,N,sum;
printf("Enter the value of N:");
scanf("%d",&N);
sum=0;
for(i=1;i<=N;i++)
sum= sum+ i;
printf("Sum of the series is: %d\n",sum);
return 0;
}



/*2) C program to find sum of the square of all natural numbers from 1
to N.
Series: 1^2+2^2+3^2+4^2+..N^2*/
#include<stdio.h>
int main()
{
	int n,s=0;
	printf("Enter the value of n :\n");
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		s=s+(i*i);
	}
	printf("Total sum = %d",s);
	return 0;
}
