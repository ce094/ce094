//Develop a program to print the transpose of a matrix.
#include<stdio.h>
int main()
{
int i,j,n,m,a[20][20];
printf("Enter the order of the matrix(m,n).");
scanf("%d %d",&m,&n);
printf("Enter the Elments of the Matrix.");
for(i=0;i<m;i++)
{
for(j=0;j<n;j++)
scanf("%d",&a[i][j]);
}
printf("The Original Matrix:\n" );
for(i=0;i<m;i++)
{
for(j=0;j<n;j++)
{
printf("%d ",a[i][j]);
}
printf("\n");
}
printf("The Transposed Matrix:\n");
for(i=0;i<n;i++)
{
for(j=0;j<m;j++)
{
printf("%d ",a[j][i]);
}
printf("\n");
}
return 0;
}
