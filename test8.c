//Illustrate pointers in swapping two numbers.
#include<stdio.h>
void swap(int*,int*);
int main()
{
	int a,b;
	int *ptr1;int *ptr2;
	ptr1=&a;
	ptr2=&b;
	printf("Please Enter Two Intergers:");
	scanf("%d %d",&a,&b);
	printf("Before Swap :%d %d\n",a,b);
	swap(ptr1,ptr2);
	printf("After Swap:%d %d",a,b);
	return 0;
}
void swap(int *x,int *y)
{
	int temp;
	temp=*x;
	*x=*y;
	*y=temp;
}
	
	
