//Develop a C program to create student structure, read two student details( Student roll number, name, section, department, fees, and results i.e., total marks obtained) and print the student details who has scored the highest.
#include<stdio.h>
struct student
{
    int sno;
    char sname[15];
    char ssec[2];
    int sfee;
    char sdepar[10];
    int smarks;
};
int main()
{
    struct student s[2];
    int i;
    for(i=0;i<2;i++)
    {
    printf("Please enter the following student details.\n");
    printf("Student Roll No:");
    scanf("%d",&s[i].sno);
    printf("Student Name :");
    scanf("%s",s[i].sname);
    printf("Student Section:");
    scanf("%s",s[i].ssec);
    printf("Student fee:");
    scanf("%d",&s[i].sfee);
    printf("Student Department:");
    scanf("%s",s[i].sdepar);
    printf("Student Marks:");
    scanf("%d",&s[i].smarks);
    printf("\n");
    }
    if(s[0].smarks>s[1].smarks)
    {
		 printf("THE DETAILS OF THE STUDENT WHO HAS SCORED HIGHEST MARKS.\n");
         printf("Student Roll No: %d\n",s[0].sno);
         printf("Student Name :%s\n",s[0].sname);
         printf("Student Section:%s\n",s[0].ssec);
         printf("Student fee:%d\n",s[0].sfee);
         printf("Student Department:%s\n",s[0].sdepar);
         printf("Student Marks:%d\n",s[0].smarks);
    }
    if(s[1].smarks>s[0].smarks)
    {
		 printf("THE DETAILS OF THE STUDENT WHO HAS SCORED HIGHEST MARKS.\n");
         printf("Student Roll No: %d\n",s[1].sno);
         printf("Student Name :%s\n",s[1].sname);
         printf("Student Section:%s\n",s[1].ssec);
         printf("Student fee:%d\n",s[1].sfee);
         printf("Student Department:%s\n",s[1].sdepar);
         printf("Student Marks:%d\n",s[1].smarks);
    }
    return 0;
    
}
