//1. Write a program to read characters until -1 is encountered, count the number of uppercase, lowercase and numbers entered and print the same.
#include <stdio.h>

#include<ctype.h>

#include<string.h>

int main()

{

  char ch,s[100];

  int a=0,A=0,n=0,i=0,l;

   while(ch!=45)

   {

     printf ("enter a character\n");

     scanf(" %c",&ch);

     s[i]=ch;

     if(isupper(ch))

     A++;

     if(islower(ch))

     a++;

     if(isdigit(ch))

     n++;

     i++;

   }

   printf("there are\n%d number of lower case letter\n%d number of upper case letters\n%d number of numbers\nand your inputs are:\n",a,A,n);

   l=strlen(s);

   for(i=0;i<=l-2;i++)

   {

       printf("%c ",s[i]);

   }

    return 0;

}





//2. Write a program and flowchart to classify a given number as prime or composite.
#include<stdio.h>
#include<conio.h>
int main()
{
	int a,m=0;
	printf("Enter the number :\n");
	scanf("%d",&a);
	for(int i=2;i<a;i++)
	{
		if(a%i==0)
		{
			m=m+i;
		}
	}
	if(m>=1)
	{
		printf("The number is c");
	}
    else
	{
		printf("The number is p");
	}
	return 0;
}


//3. Write a program to convert a decimal number to a binary number.
#include<stdio.h>
int main()
{
	int a,m=0,z=0,i=1;
	printf("Enter the number :\n");
	scanf("%d",&a);
	while(a!=0)
	{
		m=a%2;
		a=a/2;
		z=z+(i*m);
		i=i*10;
	}
	printf("%d",z);
	return 0;
}


//4. Write a program to calculate sum of squares of first n even numbers.
#include<stdio.h>
int main()
{
	int n,s=0;
	printf("Enter the value of n :\n");
	scanf("%d",&n);
	for(int i=1;i<=n;i++)
	{
		s=s+(i*i);
	}
	printf("Total sum = %d",s);
	return 0;
}



//5. Write a program to check whether the given number is palindrome or not.
#include<stdio.h>
int main()
{
	int a,m=0,i=10,s=0,z;
	printf("Enter the number : ");
	scanf("%d",&a);
	z=a;
	while(a!=0)
	{
		m=a%10;
		a=a/10;
		s=m+(i*s);
	}
	if(s==z)
	{
		printf("it's a palindrome");
	}
	else
	{
		printf("it's not a palindrome");
	}
	return 0;
}
