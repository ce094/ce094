//LINEAR SEARCH
#include<stdio.h>
int main()
{
int a[25],i,ele;
int n;
printf("Enter the limit:");
scanf("%d",&n);
printf("Enter the array of %d digits:",n);
for(i=0;i<n;i++)
scanf("%d",&a[i]);
printf("Enter the search element:");
scanf("%d",&ele);
for(i=0;i<n;i++)
{
if(ele==a[i])
{
printf("The searh element %d is found at location %d.",ele,i+1);
break;
}
}
if(i==n)
printf("The search element %d is not found in the array.",ele);
return 0;
}


//BINARY SEARCH
#include<stdio.h>
int main()
{
int a[25],mid,ele,n,i,beg,end;
printf("Enter the limit :");
scanf("%d",&n);
printf("Please enter the sorted array of %d digits :",n);
for(i=0;i<n;i++)
scanf("%d",&a[i]);
printf("Please enter the search element :");
scanf("%d",&ele);
beg=0;
end=n-1;
mid=(beg+end)/2;
while(beg<=end)
{
if(ele==a[mid])
{
printf("The search element %d is found at location : %d",ele,mid+1);
break;
}
else if(ele>a[mid])
{
beg=mid+1;
mid=(beg+end)/2;
}
else
{
end=mid-1;
mid=(beg+end)/2;
}
}
if(beg>end)
printf("The search element %d is not found in the array.",ele);
return 0;
}


