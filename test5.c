//Demonstrate reading a two-dimensional array marks which stores marks of 5 students in 3 subjects and display the highest marks in each subject.
#include<stdio.h>
int main()
{
int i,j,a[5][3],temp1=0,temp2=0,temp3=0;
printf("Enter Mathematics marks of all the 5 students :");
for(i=0;i<1;i++)
{
for(j=0;j<5;j++)
{
scanf("%d",&a[0][j]);
if(a[0][j]>temp1)
temp1=a[0][j];
}
}
printf("Enter CCP marks of all the 5 students :");
for(i=1;i<2;i++)
{
for(j=0;j<5;j++)
{
scanf("%d",&a[1][j]);
if(a[1][j]>temp2)
temp2=a[1][j];
}
}
printf("Enter Physics marks of all the 5 students :");
for(i=2;i<3;i++)
{
for(j=0;j<5;j++)
{
scanf("%d",&a[2][j]);
if(a[2][j]>temp3)
temp3=a[2][j];
}
}
printf("The MarkSheet :\n");
printf("[Columns Corresponds to students and Rows Corresponds to Subjucts.]\n"); 
for(i=0;i<3;i++)
{
for(j=0;j<5;j++)
{
printf("%d ",a[i][j]);
}
printf("\n");
}
printf("The Highest marks in Mathematics: %d\n",temp1); 
printf("The Highest marks in CCP: %d\n",temp2);
printf("The Highest marks in Physics: %d\n",temp3);
return 0;
}
